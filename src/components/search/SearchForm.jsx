import React, { Component } from 'react'
import { Input, Col, Row, Button } from 'antd'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components';

const SearchFormWrapper = styled.div`
  height: 100%;
    width: 100%;
    display: none;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9); /* Black with a little bit see-through */;
`;


class SearchForm extends Component {
  constructor (props) {
      super(props)
      this.state = {
        value: '',
        queryParms:{},
      }
    }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({ value: event.target.value })
    const output = document.createElement('div');
    const data = [...event.target.elements].reduce((data, element) => {
      if (element.name && element.value) {
        data[element.name] = element.value;
        this.setState({ value: element.value })
      }
      return data;
    }, {});
    this.setState({queryParms: data})
    output.textContent = JSON.stringify(data);
    document.body.appendChild(output);
  };
  render() {
      const {  value: query } = this.state
      console.log(this.state.value)
    return (

    <div>
      <form style={{ width: '100%', padding: 20 }} bodyStyle={{ padding: 20 }} action={`/search/${query}`} onSubmit={this.handleSubmit}>
        <input type="text" name="query" />
        <select name="documentType">
          <option value="" selected>Document Type</option>
          <option value="opinion">Opinion</option>
          <option value="person">Person</option>
        </select>
        <select name="resourceType">
          <option value="" selected>Court</option>
          <option value="wisconsin_administrative_code">Wisconsin Administrative Code</option>
          <option value="new_york_consolidated_laws">new_york_consolidated_laws</option>
        </select>
        <select name="state">
                  <option value="" selected>State</option>
                  <option value="federal">Federal</option>
                  <option value="alabama">Alabama</option>
                  <option value="florida">Florida</option>
                  <option value="ohio">Ohio</option>
                </select>
        <button>Find</button>
      </form>
      {query &&
<Redirect to={`/search/${query}`} push />}


      </div>
    );
  }
}

export default SearchForm;