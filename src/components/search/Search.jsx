import React, { Component } from 'react'
import SearchResultCard from './SearchResultCard'
import SearchForm from './SearchForm';
import { Row, Col } from 'antd'
import uuidv4 from 'uuid/v4'
import axios from "axios";
import {API_BASE_URL} from 'constants';
import styled from 'styled-components'
import { Pagination } from 'antd';
const TitleWrapper = styled.div`
    margin-top: 10%;
    margin-bottom: 12%;
    text-align: center;
    font-family: 'Asap', sans-serif;
    font-size: 5em;
`;

export default class Search extends Component {
  constructor (props) {
    super(props)
    this.state = {

      results: [],
      currentPage: 1,
    }
  }

  componentWillReceiveProps (nextProps) {
    this.handleApiCall(nextProps)
  }

  componentDidMount () {
    this.handleApiCall(this.props)
  }

 onChange = page => {
        console.log(page);
        this.setState((state) => {
        return {currentPage: page}
        });

        this.handleApiCall(this.props);

      };

  handleApiCall (props) {
    let query =  props.match.params.query;
    let page = this.state.currentPage;
    let size = 10;
    let searchUrl = `http://localhost:8080/api/documents?q=${query}&page=${page}&size=${size}`;

    //let searchUrl =API_BASE_URL + `/documents?q=${query}`;
    axios.get(searchUrl)
      .then(response => {
        const state = Object.assign({}, this.state);
        this.setState({
          results: response.data,
          totalResults: response.data.size,
        })
         console.log(searchUrl);
        console.log(response.data)
      }).catch((error) => {
      if (axios.isCancel(error) || error) {
        this.setState({
          loading: false,
          message: 'Failed to fetch results.Please check network',
        });
      }
    }).catch(error => {
      console.error('Error path:', error.response);
    });
  }




  render () {
  const dataSource = [];


     if (!this.state.results){
      return <div></div>
    }
    return (
      <div>
       Search results for {this.props.match.params.query}
      {<SearchForm/>}
      { <Row gutter={24}>
          {
            this.state.results.map(result => {
              return (
                <Col className='gutter-row' span={50} offset={1} key={uuidv4()}>
                  <SearchResultCard
                    title={result.content.title}
                    url={result.content.url}
                    resourceType={result.content.resourceType}
                    date={result.content.date}
                    id={result.id}
                  />
                </Col>
              )
            })
          }
        </Row> }
        {<Pagination current={this.state.currentPage} onChange={this.onChange} total={50} />}
      </div>
    )
  }
}
