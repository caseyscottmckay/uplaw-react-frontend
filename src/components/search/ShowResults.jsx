import React, { Component } from 'react'
import SearchResultCard from './SearchResultCard'
import { Row, Col } from 'antd'
import uuidv4 from 'uuid/v4'
import './ShowResults.css'
import axios from "axios";
import { Table } from 'antd';
import {API_BASE_URL} from 'constants';
import styled from 'styled-components';

const TitleWrapper = styled.div`
    margin-top: 10%;
    margin-bottom: 12%;
    text-align: center;
    font-family: 'Asap', sans-serif;
    font-size: 5em;
`;

export default class ShowResults extends Component {
  constructor (props) {
    super(props)
    this.state = {
      results: [],
      selectedRowKeys: []
    }
  }
  selectRow = (record) => {
      const selectedRowKeys = [...this.state.selectedRowKeys];
      if (selectedRowKeys.indexOf(record.key) >= 0) {
        selectedRowKeys.splice(selectedRowKeys.indexOf(record.key), 1);
      } else {
        selectedRowKeys.push(record.key);
      }
      this.setState({ selectedRowKeys });
    }
    onSelectedRowKeysChange = (selectedRowKeys) => {
      this.setState({ selectedRowKeys });
    }

  handleApiCall (props) {
    let query = props.match.params.query
    console.log(query)
    let searchUrl = `http://localhost:8080/api/documents?q=${query}`;
    //let searchUrl =API_BASE_URL + `/documents?q=${query}`;
    axios.get(searchUrl)
      .then(response => {
        const state = Object.assign({}, this.state);
        state.documents = response.data;
        this.setState(state);

        this.setState({
          results: response.data
        })
        console.log(response.data)

      }).catch((error) => {
      if (axios.isCancel(error) || error) {
        this.setState({
          loading: false,
          message: 'Failed to fetch results.Please check network',
        });
      }
    }).catch(error => {
      console.error('Error path:', error.response);
    });
  }

  componentWillReceiveProps (nextProps) {
    this.handleApiCall(nextProps)
  }

  componentDidMount () {
    this.handleApiCall(this.props)
  }



  render () {
  const dataSource = [];

    const columns = [{
      title: 'Id',
      dataIndex: 'id',
      render: (text, record) => <a href={'/documents/' +text}>{text}</a>,
    }, {
      title: 'URL',
      dataIndex: 'content.url',
      render: (text, record) => <a href={'/documents' +text}>{text}</a>,
    }, {
      title: 'Title',
      dataIndex: 'content.title',
    }, {
      title: 'Resource Type',
      dataIndex: 'content.resourceType.name',
    }, {
      title: 'Court',
      dataIndex: 'content.resourceType.jurisdiction',
    }, {
      title: 'Date Created',
      dataIndex: 'content.dateCreated'
    }];



    if (!this.state.documents){
      return <div></div>
    }
const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectedRowKeysChange,
    };
    return (

      <div>

       Search results for {this.props.match.params.query}

       <Table
               rowSelection={rowSelection}
               columns={columns}
               dataSource={this.state.results}
               onRow={(record) => ({
                 onClick: () => {
                   this.selectRow(record);
                 },
               })}
             />

        }


        {/* { <Row gutter={24}>
          {
            this.state.results.map(result => {
              return (
                <Col className='gutter-row' span={50} offset={1} key={uuidv4()}>
                  <SearchResultCard
                    name={result.title}
                    date={result.release_date}
                    pollPollVote={result.pollPollVote_average}
                    image={result.poster_path}
                    id={result.id}
                  />
                </Col>
              )
            })
          }
        </Row> } */}

      </div>
    )
  }
}