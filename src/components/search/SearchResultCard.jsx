import React from 'react'
import { Link } from 'react-router-dom'
import { Card } from 'antd'
import './SearchResultsCard.css';
import styled from 'styled-components';


const TitleWrapper = styled.div`
    margin-top: 10%;
    margin-bottom: 12%;
    text-align: center;
    font-family: 'Asap', sans-serif;
    font-size: 5em;
`;
const CustomCard = styled.div`
    margin-top: 10%;
    margin-bottom: 12%;
    text-align: center;
    font-family: 'Asap', sans-serif;
    font-size: 5em;
`;
type Props = {
  title: string,
  url: string,
  date: string,
  resourceType: string,
  id: number
}

const SearchResultCard = ({ title, url, date, resourceType, image, id }: Props) => (

    <Card style={{ width: '100%' }} bodyStyle={{ padding: 0 }}>
      <div className='custom-image'>
      </div>
      <div className='custom-card'>
        <h3><Link to={`/documents/${id}`}>{title}</Link></h3>
        <p>{resourceType.name}</p>
      </div>
    </Card>

)

export default SearchResultCard